@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <form action="{{ route('pessoas.store') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="nome">Nome</label>
                    {!! Form::text('nome', null, ['class' =>'form-control']) !!}
                    {{-- <input type="text" id="nome" class="form-control" name="nome"> --}}
                </div>

                <div class="form-group">
                    <label for="sobrenome">Sobrenome</label>
                    <input type="text" id="sobrenome" class="form-control" name="sobrenome">
                </div>
                <div class="form-group">
                    <label for="sobrenome">Sexo</label>
                    <select name="sexo" id="sexo" class="form-control">
                        <option value="M">Masculino</option>
                        <option value="F">Feminino</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
